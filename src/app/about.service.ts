import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { About} from './about';

@Injectable()
export class AboutService {
    private aboutUrl = 'http://prasannashiwakoti.com:3000/api/about';
   
    constructor(private http: Http) { }
   
    getAbouts(): Promise<About []>{
        return this.http.get(this.aboutUrl)
            .toPromise()
            .then(res => res.json() as About [])
            .catch(this.handleError);
    }
    
    // in a real program need to do more than just log problems
    private handleError(error: any): Promise<any> {
        console.error('An error occurred !!!', error);
        return Promise.reject(error.message || error);
    }    
    
}