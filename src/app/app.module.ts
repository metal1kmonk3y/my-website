import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { HomeComponent } from './homeComponent';
import { ResumeComponent } from './resumeComponent';
import { ProjectsComponent } from './projectsComponent';
import { AboutComponent } from './aboutComponent';
import { ContactComponent } from './contactComponent';

import { AboutService } from './about.service';
import { ProjectService } from './project.service';


import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ResumeComponent,
    ProjectsComponent,
    AboutComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [AboutService, ProjectService],
  bootstrap: [AppComponent]
})
export class AppModule { }
