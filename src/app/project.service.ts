import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Project} from './project';

@Injectable()
export class ProjectService {
    private projectsUrl = 'http://prasannashiwakoti.com:3000/api/projects';
   
    constructor(private http: Http) {}
   
    getProjects(): Promise<Project []>{
        return this.http.get(this.projectsUrl)  
            .toPromise()
            .then(res => res.json() as Project [])
            .catch(this.handleError);
    }
    
    // in a real program need to do more than just log problems
    private handleError(error: any): Promise<any> {
        console.error('An error occurred !!!', error);
        return Promise.reject(error.message || error);
    }
}