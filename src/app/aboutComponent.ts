import { Component, OnInit } from '@angular/core';


import { About} from './about';

import { AboutService} from './about.service';

@Component({
    selector: 'my-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss']
})

export class AboutComponent implements OnInit {
    
    abouts : About [];
    
    constructor(private aboutService: AboutService){       
    }
    
    getAbouts(): void {
        this.aboutService.getAbouts().then(abouts => this.abouts = abouts);
        
    }

    
    ngOnInit(): void {
        this.getAbouts();
    }
}