import { NgModule } from '@angular/core';
import {RouterModule, Routes } from '@angular/router';


import { HomeComponent } from './homeComponent';
import { ResumeComponent } from './resumeComponent';
import { ProjectsComponent } from './projectsComponent';
import { AboutComponent } from './aboutComponent';
import { ContactComponent } from './contactComponent';

const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'resume',
        component: ResumeComponent
    },
    {
        path: 'projects',
        component: ProjectsComponent
    },
    {
        path: 'about',
        component: AboutComponent
    },
    {
        path: 'contact',
        component: ContactComponent
    },
    {
        path: '',
        redirectTo : '/home',
        pathMatch: 'full'
    },
    {
        path: '**', 
        component: HomeComponent 
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {}