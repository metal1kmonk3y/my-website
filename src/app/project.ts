export class Project {
    _id: string;
    name: string;
    url: string;
    repo: string;
    description: string;

}